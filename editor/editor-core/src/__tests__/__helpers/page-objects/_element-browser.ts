export const elementBrowserSelectors = {
  elementBrowser: '[data-testid="element-browser"]',
  viewMore: '[data-testid="view-more-elements-item"]',
  modalBrowser: '[role="dialog"]',
};
