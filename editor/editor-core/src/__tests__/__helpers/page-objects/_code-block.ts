export const codeBlockSelectors = {
  lineNumbers: '.line-number-gutter',
  content: '.code-content',
  code: '.code-block code',
  languageSelectInput:
    'div[aria-label="CodeBlock floating controls"] input[aria-autocomplete="list"]',
  copyToClipboardButton: 'button[aria-label="Copy"]',
};
