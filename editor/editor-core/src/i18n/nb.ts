/**
 * NOTE:
 *
 * This file is automatically generated by i18n-tools.
 * DO NOT CHANGE IT BY HAND or your changes will be lost.
 */
// Norwegian Bokmål
export default {
  'fabric.editor.action': 'Handlingselement',
  'fabric.editor.action.description': 'Opprett og tilordne handlinger',
  'fabric.editor.addAltText': 'Alternativ tekst',
  'fabric.editor.alignImageCenter': 'Midtjustert',
  'fabric.editor.alignImageLeft': 'Venstrejustert',
  'fabric.editor.alignImageRight': 'Høyrejustert',
  'fabric.editor.alignment': 'Justering',
  'fabric.editor.altText': 'Alternativ tekst',
  'fabric.editor.alttext.validation': 'Fjern alle spesialtegn i alt-teksten.',
  'fabric.editor.annotate': 'Kommenter',
  'fabric.editor.annotationToolbar': 'Verktøylinje for kommentarer',
  'fabric.editor.backLink': 'Gå tilbake',
  'fabric.editor.blockCardUnavailable':
    'Inline-koblingen er inne i {node}, og visningen kan ikke endres',
  'fabric.editor.blockquote.description': 'Sett inn et sitat',
  'fabric.editor.bold': 'Fet',
  'fabric.editor.canNotSortTable':
    '⚠️ Du kan ikke sortere en tabell med sammenslåtte celler',
  'fabric.editor.cancelButton': 'Avbryt',
  'fabric.editor.cardFloatingControls': 'Kortalternativer',
  'fabric.editor.cellBackground': 'Cellebakgrunn',
  'fabric.editor.cellOptions': 'Cellealternativer',
  'fabric.editor.clearAltTextEdit': 'Tøm alternativ tekst',
  'fabric.editor.clearCells': '{0, plural, one {Tøm celle} other {Tøm celler}}',
  'fabric.editor.clearFormatting': 'Slett formatering',
  'fabric.editor.clearLink': 'Fjern kobling',
  'fabric.editor.clearLinkText': 'Fjern tekst',
  'fabric.editor.closeAltTextEdit': 'Tilbake',
  'fabric.editor.closeFindReplaceDialog': 'Lukk',
  'fabric.editor.closeHelpDialog': 'Lukk hjelpedialog',
  'fabric.editor.code': 'Kode',
  'fabric.editor.codeblock': 'Kodebit',
  'fabric.editor.codeblock.description': 'Vis kode med syntaksutheving',
  'fabric.editor.columns': 'Oppsett',
  'fabric.editor.columns.description': 'Konstruer siden med seksjoner',
  'fabric.editor.configPanel.cancel': 'Avbryt',
  'fabric.editor.configPanel.fieldTypeError.isMultipleAndRadio':
    'Kan ikke kombinere isMultiple med stil: radio',
  'fabric.editor.configPanel.formType.addField': 'Legg til felt',
  'fabric.editor.configPanel.formType.removeField': 'Fjern felt',
  'fabric.editor.configPanel.required': 'Obligatorisk felt',
  'fabric.editor.configPanel.submit': 'Send inn',
  'fabric.editor.date': 'Dato',
  'fabric.editor.date.description': 'Legg til en dato med en kalender',
  'fabric.editor.decision': 'Beslutning',
  'fabric.editor.decision.description':
    'Registrer beslutninger slik at det er lett å spore dem',
  'fabric.editor.decisionPlaceholder': 'Legg til en beslutning …',
  'fabric.editor.displayInline': 'Vis inline',
  'fabric.editor.displayLink': 'Vis som tekst',
  'fabric.editor.displayText': 'Tekst som skal vises',
  'fabric.editor.edit': 'Rediger',
  'fabric.editor.editAltText': 'Rediger alternativ tekst',
  'fabric.editor.editLink': 'Rediger kobling',
  'fabric.editor.editMode.inviteToEditButton.title': 'Inviter til å redigere',
  'fabric.editor.editorHelp': 'Editorhjelp',
  'fabric.editor.emoji': 'Emoji',
  'fabric.editor.emoji.description':
    'Bruk emojier for å uttrykke tanker 🎉 og følelser 😄',
  'fabric.editor.error': 'Feil',
  'fabric.editor.errorPanel': 'Feilpanel',
  'fabric.editor.errorPanel.description': 'Trekk frem feil i et farget panel',
  'fabric.editor.expand': 'Utvidelse',
  'fabric.editor.expand.description': 'Sett inn en utvidelse',
  'fabric.editor.feedbackDialog': 'Gi en tilbakemelding',
  'fabric.editor.feedbackDialog.description':
    'Fortell oss hvordan du opplevde det nye redigeringsprogrammet',
  'fabric.editor.filesAndImages': 'Filer og bilder',
  'fabric.editor.filesAndImages.description':
    'Legg til bilder og andre filer på siden',
  'fabric.editor.find': 'Finn',
  'fabric.editor.findNext': 'Finn neste',
  'fabric.editor.findPrevious': 'Finn forrige',
  'fabric.editor.findReplaceToolbarButton': 'Finn og erstatt',
  'fabric.editor.headerColumn': 'Overskriftskolonne',
  'fabric.editor.headerRow': 'Overskriftsrad',
  'fabric.editor.heading1': '1. overskrift',
  'fabric.editor.heading1Description':
    'Bruk dette for en overskrift på toppnivå',
  'fabric.editor.heading2': '2. overskrift',
  'fabric.editor.heading2Description': 'Bruk dette til viktige avsnitt',
  'fabric.editor.heading3': '3. overskrift',
  'fabric.editor.heading3Description':
    'Bruk dette til underavsnitt og overskrifter for grupper',
  'fabric.editor.heading4': '4. overskrift',
  'fabric.editor.heading4Description': 'Bruk dette til dype overskrifter',
  'fabric.editor.heading5': '5. overskrift',
  'fabric.editor.heading5Description':
    'Bruk dette til å gruppere listeelementer',
  'fabric.editor.heading6': '6. overskrift',
  'fabric.editor.heading6Description':
    'Bruk dette for en overskrift på nedre nivå',
  'fabric.editor.help': 'Hjelp',
  'fabric.editor.help.description':
    'Se gjennom alle tastatursnarveier og Markdown-alternativer',
  'fabric.editor.helpDialogTips': 'Trykk på {keyMap} for å åpne denne dialogen',
  'fabric.editor.horizontalRule': 'Deler',
  'fabric.editor.horizontalRule.description':
    'Atskill innhold med en horisontal linje',
  'fabric.editor.hyperlinkToolbarPlaceholder':
    'Lim inn kobling eller søk etter nylig vist',
  'fabric.editor.image': 'Bilde',
  'fabric.editor.info': 'Informasjon',
  'fabric.editor.infoPanel': 'Informasjonspanel',
  'fabric.editor.infoPanel.description':
    'Uthev informasjonen i et farget panel',
  'fabric.editor.insertMenu': 'Sett inn',
  'fabric.editor.italic': 'Kursiv',
  'fabric.editor.keyboardShortcuts': 'Tastatursnarveier',
  'fabric.editor.layoutFixedWidth': 'Tilbake til midten',
  'fabric.editor.layoutFullWidth': 'Gå for full bredde',
  'fabric.editor.layoutWide': 'Gå for bredere',
  'fabric.editor.leftSidebar': 'Sidelinje til venstre',
  'fabric.editor.link': 'Kobling',
  'fabric.editor.link.description': 'Sett inn en kobling',
  'fabric.editor.linkAddress': 'Koblingsadresse',
  'fabric.editor.linkPlaceholder': 'Lim inn kobling',
  'fabric.editor.lists': 'Lister',
  'fabric.editor.markdown': 'Markdown',
  'fabric.editor.mediaAddLink': 'Legg til kobling',
  'fabric.editor.mention': 'Nevn',
  'fabric.editor.mention.description': 'Oppgi noen for å sende dem et varsel',
  'fabric.editor.mergeCells': 'Slå sammen celler',
  'fabric.editor.moreFormatting': 'Mer formatering',
  'fabric.editor.normal': 'Normal tekst',
  'fabric.editor.note': 'Merknad',
  'fabric.editor.notePanel': 'Kommentarpanel',
  'fabric.editor.notePanel.description':
    'Legg til en merknad i et farget panel',
  'fabric.editor.numberedColumn': 'Nummert kolonne',
  'fabric.editor.orderedList': 'Nummerert liste',
  'fabric.editor.orderedList.description': 'Opprett en sortert liste',
  'fabric.editor.other': 'Andre …',
  'fabric.editor.pastePlainText': 'Lim inn ren tekst',
  'fabric.editor.placeholderAltText':
    'Beskriv dette bildet med alternativ tekst',
  'fabric.editor.placeholderText': 'Plassholdertekst',
  'fabric.editor.placeholderTextPlaceholder': 'Legg til plassholdertekst',
  'fabric.editor.quickInsert': 'Hurtiginnsetting',
  'fabric.editor.redo': 'Gjør om',
  'fabric.editor.remove': 'Fjern',
  'fabric.editor.replace': 'Erstatt',
  'fabric.editor.replaceAll': 'Erstatt alle',
  'fabric.editor.replaceWith': 'Erstatt med',
  'fabric.editor.rightSidebar': 'Sidelinje til høyre',
  'fabric.editor.saveButton': 'Lagre',
  'fabric.editor.selectLanguage': 'Velg språk',
  'fabric.editor.selected': 'Valgt',
  'fabric.editor.sortColumnASC': 'Sorter kolonne A → Å',
  'fabric.editor.sortColumnDESC': 'Sorter kolonne Å → A',
  'fabric.editor.splitCell': 'Del celle',
  'fabric.editor.status': 'Status',
  'fabric.editor.status.description': 'Legg til en egendefinert statusetikett',
  'fabric.editor.statusPlaceholder': 'Angi en status',
  'fabric.editor.strike': 'Gjennomstreking',
  'fabric.editor.subscript': 'Senket skrift',
  'fabric.editor.success': 'Vellykket',
  'fabric.editor.successPanel': 'Utførtpanel',
  'fabric.editor.successPanel.description': 'Legg til tips i et farget panel',
  'fabric.editor.superscript': 'Hevet skrift',
  'fabric.editor.supportAltText':
    'Alternativ tekst er nyttig for folk som bruker skjermlesere på grunn av redusert synsfunksjon.',
  'fabric.editor.table': 'Tabell',
  'fabric.editor.table.description': 'Sett inn en tavle',
  'fabric.editor.tableOptions': 'Tabellalternativer',
  'fabric.editor.taskPlaceholder':
    'Skriv inn handlingen. Bruk @ for å tilordne den til noen.',
  'fabric.editor.textColor': 'Tekstfarge',
  'fabric.editor.textColor.lessColors': 'Færre farger',
  'fabric.editor.textColor.moreColors': 'Flere farger',
  'fabric.editor.textStyles': 'Tekststiler',
  'fabric.editor.threeColumns': 'Tre kolonner',
  'fabric.editor.threeColumnsWithSidebars': 'Tre kolonner med sidelinjer',
  'fabric.editor.twoColumns': 'To kolonner',
  'fabric.editor.underline': 'Understreking',
  'fabric.editor.undo': 'Angre',
  'fabric.editor.unlink': 'Fjern kobling',
  'fabric.editor.unorderedList': 'Punktliste',
  'fabric.editor.unorderedList.description': 'Opprett en uordnet liste',
  'fabric.editor.viewMore': 'Vis mer',
  'fabric.editor.visit': 'Åpne koblingen i et nytt vindu',
  'fabric.editor.warning': 'Advarsel',
  'fabric.editor.warningPanel': 'Advarselspanel',
  'fabric.editor.warningPanel.description':
    'Legg til en advarsel i et farget panel',
  'fabric.editor.wrapLeft': 'Juster til venstre',
  'fabric.editor.wrapRight': 'Juster til høyre',
  'fabric.theme.blue': 'Blue',
  'fabric.theme.dark-blue': 'Mørk blå',
  'fabric.theme.dark-green': 'Mørk grønn',
  'fabric.theme.dark-purple': 'Mørk lilla',
  'fabric.theme.dark-red': 'Mørkerød',
  'fabric.theme.dark-teal': 'Mørk blågrønn',
  'fabric.theme.dark-yellow': 'Dark yellow',
  'fabric.theme.gray': 'Gray',
  'fabric.theme.green': 'Green',
  'fabric.theme.light-blue': 'Light blue',
  'fabric.theme.light-gray': 'Light gray',
  'fabric.theme.light-green': 'Lysegrønn',
  'fabric.theme.light-purple': 'Lys lilla',
  'fabric.theme.light-red': 'Lyserød',
  'fabric.theme.light-teal': 'Lys blågrønn',
  'fabric.theme.light-yellow': 'Light yellow',
  'fabric.theme.orange': 'Oransje',
  'fabric.theme.purple': 'Lilla',
  'fabric.theme.red': 'Rød',
  'fabric.theme.teal': 'Blågrønn',
  'fabric.theme.white': 'Hvit',
  'fabric.theme.yellow': 'Yellow',
};
