import { B400, B50, N10, N30, N500 } from '@atlaskit/theme/colors';
export const navigationBackgroundColor = N10;

export const itemHoverBackgroundColor = N30;

export const itemActiveBackgroundColor = B50;
export const itemTextColor = N500;
export const itemTextSelectedColor = B400;

export const separatorColor = N30;
