export { default } from './components/ModalWrapper';
export { default as ModalTransition } from './components/ModalTransition';
export {
  Body as ModalBody,
  Header as ModalHeader,
  Footer as ModalFooter,
  Title as ModalTitle,
} from './styled/Content';
export type {
  BodyProps as BodyComponentProps,
  TitleTextProps as TitleComponentProps,
} from './styled/Content';
export type {
  KeyboardOrMouseEvent,
  ActionProps,
  ScrollBehavior,
  ContainerComponentProps,
} from './types';
export type { FooterComponentProps } from './components/Footer';
export type { HeaderComponentProps } from './components/Header';
