import { default as Icon, IconProps } from './components/Icon';
export default Icon;
export type { IconProps };
export { sizeMap as size, sizes } from './constants';
export { default as Skeleton } from './components/Skeleton';
