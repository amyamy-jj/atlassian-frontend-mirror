"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sizeMap = exports.sizes = void 0;
exports.sizes = {
    small: '16px',
    medium: '24px',
    large: '32px',
    xlarge: '48px',
};
exports.sizeMap = {
    small: 'small',
    medium: 'medium',
    large: 'large',
    xlarge: 'xlarge',
};
//# sourceMappingURL=constants.js.map