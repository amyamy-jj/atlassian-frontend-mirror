"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Icon_1 = tslib_1.__importDefault(require("./components/Icon"));
exports.default = Icon_1.default;
var constants_1 = require("./constants");
Object.defineProperty(exports, "size", { enumerable: true, get: function () { return constants_1.sizeMap; } });
Object.defineProperty(exports, "sizes", { enumerable: true, get: function () { return constants_1.sizes; } });
var Skeleton_1 = require("./components/Skeleton");
Object.defineProperty(exports, "Skeleton", { enumerable: true, get: function () { return Skeleton_1.default; } });
//# sourceMappingURL=index.js.map