"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Icon = _interopRequireDefault(require("../../cjs/components/Icon"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

const Image48Icon = props => /*#__PURE__*/_react.default.createElement(_Icon.default, _extends({
  dangerouslySetGlyph: `<svg width="48" height="64" viewBox="0 0 48 64" focusable="false" role="presentation"><g fill-rule="evenodd"><path fill="#FFF" stroke="#091E42" stroke-opacity=".08" d="M4 .5h28.007a3.5 3.5 0 012.52 1.072l11.994 12.45a3.5 3.5 0 01.979 2.429V60a3.5 3.5 0 01-3.5 3.5H4A3.5 3.5 0 01.5 60V4A3.5 3.5 0 014 .5z"/><path fill="#FFAB00" d="M21.667 37.833l6.258-6.258a1 1 0 011.447.034l3.961 4.358v4.2H14.667l3.96-3.96a1 1 0 011.413 0l1.627 1.626zm-3.89-7.777a3.111 3.111 0 110-6.223 3.111 3.111 0 010 6.223z"/></g></svg>`
}, props, {
  size: "xlarge"
}));

Image48Icon.displayName = 'Image48Icon';
var _default = Image48Icon;
exports.default = _default;