import React from 'react';

import { Checkbox } from '../../src';

export default () => (
  <Checkbox
    isRequired
    label="Checkbox is required"
    value="Checked"
    name="checkbox-required"
  />
);
