import React from 'react';

import { Status } from '../../src';

export default function AvatarStatusLockedExample() {
  return (
    <div style={{ width: 24 }}>
      <Status status="locked" />
    </div>
  );
}
