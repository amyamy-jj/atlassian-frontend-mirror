import React from 'react';

import Avatar from '../../src';

export default function AvatarXSmallExample() {
  return (
    <div>
      <Avatar size="xsmall" />
      <Avatar size="xsmall" appearance="square" />
    </div>
  );
}
