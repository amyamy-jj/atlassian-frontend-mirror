import React from 'react';

import { Skeleton } from '../../src';

export default function AvatarSkeletonCircleExample() {
  return <Skeleton appearance="circle" />;
}
