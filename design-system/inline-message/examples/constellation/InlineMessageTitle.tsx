import React from 'react';

import InlineMessage from '../../src';

export default () => (
  <InlineMessage title="Title">
    <p>Dialog</p>
  </InlineMessage>
);
